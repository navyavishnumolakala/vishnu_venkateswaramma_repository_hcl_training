create database movie_vishn;
use movie_vishn;
create table moviedataset(id int(10)primary key not null,title varchar(45),year int(10),storyline varchar(105),imdbRating int(10),Classification varchar(45));
desc moviedataset;

iNSERT INTO moviedataset VALUES (19,'Radhe Shyam',2022,'film written and directed by Radha Krishna Kumar,starring Prabhas and Pooja Hegde',7,'coming soon');
iNSERT INTO moviedataset VALUES (15,'The lion king',2017,'However, she soon realises that the class she has been assigned ',9,'top rated movies');
iNSERT INTO moviedataset VALUES (18,'Red lion',2019,' A family''s road secluded mobile home park to stay with some relatives',8,'top rated indian');
iNSERT INTO moviedataset VALUES (14,'Attack',2022,'The second half of Attack on titan seson 4 finally has a release date 2022 january ',7,'coming soon');
iNSERT INTO moviedataset VALUES (16,'Major',2021,'Adivi Sesh, Sobhita Dhulipala,Saiee.. Action and Drama Directed by Sashi kiran Tikka',8,'coming soon');
iNSERT INTO moviedataset VALUES (21,'Prithhviraj',2022,'Akash Kumar, Manushi Chllar, Biography, Drama, Directed by Chandraprakash Dwivedi',9,'coming soon');
iNSERT INTO moviedataset VALUES (22,'Panipat',2017,'Najib receives intelligence that the Marathas have retreated,they are marching north to Delhi',9,'top rated movies');
iNSERT INTO moviedataset VALUES (24,'Kabir Singh',2019,'Kabir and his friends announce that he has exclusively claimed Preeti',7,'top rated movies');
iNSERT INTO moviedataset VALUES (26,'Super 30',2018,'Anand frequently goes to Banaras Hindu University',8,'top rated movies');
iNSERT INTO moviedataset VALUES (28,'Raazi',2016,'Hidayat Khan is an Indian freedom fighter and agent posing as an informant for the India',8,'top rated indian');
iNSERT INTO moviedataset VALUES (23,'Red',2021,'The movie  cast includes Ram playing the main lead role while Mani Sharma scored music',7,'top rated indian');
iNSERT INTO moviedataset VALUES (25,'Red lion',2019,'Khan had interviewed the Phogat sisters on his talk show Satyamev Jayate',9,'top rated indian');

select *from moviedataset;
